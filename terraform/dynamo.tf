# aws_dynamodb_table
resource "aws_dynamodb_table" "temperature-table" {
  name           = "Temperature"
  hash_key      = "Id"

  attribute {
    name = "Id"
    type = "S"
  }
  read_capacity = 20
  write_capacity = 20
}